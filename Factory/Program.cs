﻿using System;
using Castle.Facilities.TypedFactory;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace Factory
{
  /// <summary>
  /// What we have:
  /// ICommandRunner defines three operations: Connect, DoCommand, Disconnect
  /// CommandRunner implements ICommandRunner to execute commands on particular node (based on nodeId)
  /// Service represents a ficional service with DoPoll(list of nodeId's)
  /// 
  /// Problem definition:
  /// We need to create a separate CommandRunner for each particular node
  /// We don't know nodeId's in advance
  /// We need to send a numeric parameter to CommandRunner's constructor
  /// 
  /// Factory with CastleWindsor:
  /// Add TypedFactoryFacility to Castle Windsor - Important: This should NOT be in an installer
  /// Create ICommandRunnerFactory interface with Create and Release operations
  /// Register the interface AsFactory - Castle Windsor will create the implementation itself
  /// Call factory.Create to create an object
  /// Use try-finally block to ensure that all objects are released with factory.Release
  /// </summary>
  public class Program
  {
    public static void Main(string[] args) {
      var container = new WindsorContainer();
      container.AddFacility<TypedFactoryFacility>();
      container.Install(FromAssembly.This());

      var service = container.Resolve<Service>();
      service.DoPoll(new [] {1,5,6,7,9});

      Console.WriteLine("Disposing the container");
      container.Dispose();
    }
  }
}
