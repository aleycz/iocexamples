﻿using System.Collections.Generic;

namespace Factory
{
    public class Service
    {

        private readonly ICommandRunnerFactory commandRunnerFactory;

        public Service(ICommandRunnerFactory commandRunnerFactory)
        {
            this.commandRunnerFactory = commandRunnerFactory;
        }

        public void DoPoll(IEnumerable<int> nodes)
        {
            foreach (int nodeId in nodes)
            {
                var runner = commandRunnerFactory.Create(nodeId);
                try
                {
                    runner.Connect();
                    runner.DoCommand("...");
                    runner.Disconnect();
                }
                finally
                {
                    //commandRunnerFactory.Release(runner);
                }
            }
        }
    }
}
