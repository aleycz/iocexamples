﻿namespace Factory {
  public interface ICommandRunnerFactory {
    ICommandRunner Create(int nodeId);
    void Release(ICommandRunner obj);
  }
}