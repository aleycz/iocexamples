﻿using System;
using System.Collections;

namespace Decorator.Decorators {
  public class AsteriskDecorator : IDataSource {

    private readonly IDataSource source;

    public AsteriskDecorator(IDataSource source) {
      this.source = source ?? throw new ArgumentNullException(nameof(source));
    }

    public IEnumerable GetData(int size) {
      foreach(object o in source.GetData(size)) {
        if(o is int number) {
          for (int i = 0; i < number; i++)
            yield return '*';
        } else {
          yield return o;
        }
      }
    }
  }
}