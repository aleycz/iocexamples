﻿using System;
using System.Collections;

namespace Decorator.Decorators {
  public class EndlineDecorator : IDataSource {

    private readonly IDataSource source;

    public EndlineDecorator(IDataSource source) {
      this.source = source ?? throw new ArgumentNullException(nameof(source));
    }

    public IEnumerable GetData(int size) {
      foreach(object o in source.GetData(size)) {
        yield return o;
        yield return "\r\n";
      }
    }
  }
}