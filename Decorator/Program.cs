﻿using System;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace Decorator
{
  /// <summary>
  /// What we have:
  /// IDataSource.GetData returns IEnumerable sequence of data
  /// NumberSequence implements IDataSource to return a sequence of integers
  /// Writer uses IDataSource to write the data to console
  /// 
  /// Now we are going to add decorators:
  /// EndlineDecorator adds a newline ("\r\n") after each item in sequence
  /// AsteriskDecorator converts are integer in the sequence to the row of asterisks of that size
  /// 
  /// We use Castle Windsor's installers to automatically setup the decorators
  /// We use FromAssembly.This to install the installer automatically
  /// Writer is a top level class, i.e. it doesn't need an interface
  /// </summary>
  public class Program
  {
    public static void Main(string[] args)
    {
      var container = new WindsorContainer();
      container.Install(FromAssembly.This());

      var writer = container.Resolve<Writer>();

      writer.Write(5);
      Console.WriteLine();

      writer.Write(7);
    }
  }
}
