﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Decorator.Decorators;

namespace Decorator {
  public class Installer : IWindsorInstaller {
    public void Install(IWindsorContainer container, IConfigurationStore store) {
      container.Register(Component.For<IDataSource>().ImplementedBy<AsteriskDecorator>());
      container.Register(Component.For<IDataSource>().ImplementedBy<EndlineDecorator>());
      container.Register(Component.For<IDataSource>().ImplementedBy<NumberSequence>());
      container.Register(Component.For<Writer>());
    }
  }
}