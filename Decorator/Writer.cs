﻿using System;

namespace Decorator {
  public class Writer
  {

    private readonly IDataSource source;

    public Writer(IDataSource source) {
      this.source = source ?? throw new ArgumentNullException(nameof(source));
    }

    public void Write(int size) {
      foreach(object o in source.GetData(size)) {
        Console.Write(o);
      }
    }
  }
}