﻿using System;

namespace Plugins.PluginA {
  public class DayOfWeek : IPlugin {

    private readonly string[] days = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

    public bool IsFor(object data) {
      return data is int number && number >= 1 && number <= 7;
    }

    public void DoHardWork(object data) {
      if(!IsFor(data)) throw new InvalidOperationException();

      Console.WriteLine(days[(int)data - 1]);
    }
  }
}