﻿using System;

namespace Plugins.PluginB {
  public class StringLength : IPlugin {
    public bool IsFor(object data) {
      return data is string;
    }

    public void DoHardWork(object data) {
      Console.WriteLine($"{data} {data.ToString().Length}");
    }
  }
}