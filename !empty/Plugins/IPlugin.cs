﻿namespace Plugins {
  public interface IPlugin {
    bool IsFor(object data);

    void DoHardWork(object data);
  }
}