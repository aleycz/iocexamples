﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

namespace Plugins
{
    /// <summary>
    /// Castle Windsor allows multiple components for the same service
    /// We can then resolve all of them as a collection of objects
    /// 
    /// What we have here:
    /// MainService supports plugins
    /// Each plugin implements IPlugin interface
    /// IPlugin interface defines IsFor(object) and DoHardWork(object) operations
    /// 
    /// How Castle Windsor helps:
    /// We must add CollectionResolver subresolver to our container
    /// Service class can now depend on IEnumerable{IPlugin} to get instances of all known plugins
    /// Registration to the container is done by Classes.FromThisAssembly().BasedOn{IPlugin}.WithService.FromInterface()
    ///
    /// How we add a new plugin:
    /// We simply add a new class implementing IPlugin
    /// That's it :-)
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {

            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.Register(Component.For<MainService>());

            var service = container.Resolve<MainService>();
            service.ProcessData(1);
            service.ProcessData(2);
            service.ProcessData(3);
            service.ProcessData(null);
            service.ProcessData("Hello");
            service.ProcessData("World!");
        }
    }
}
