﻿using System.Collections;

namespace Decorator {
  public class NumberSequence : IDataSource {

    public IEnumerable GetData(int size) {
      for(int i = 1; i <= size; i++) {
        yield return i;
      }
    }
  }
}