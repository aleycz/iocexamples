﻿using System;
using System.Collections;

namespace Decorator.Decorators
{
    public class XyzDecorator : IDataSource
    {
        private readonly IDataSource source;

        public XyzDecorator(IDataSource source)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
        }

        public IEnumerable GetData(int size)
        {
            foreach (object o in source.GetData(size))
            {
                yield return o;
            }
        }
    }
}