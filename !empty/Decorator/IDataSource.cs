﻿using System.Collections;

namespace Decorator {
  public interface IDataSource {
    IEnumerable GetData(int size);
  }
}