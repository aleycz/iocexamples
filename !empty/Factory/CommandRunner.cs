﻿using System;

namespace Factory {
  public class CommandRunner : ICommandRunner {
    private bool disposed;
    private readonly int nodeId;

    public CommandRunner(int nodeId) {
      this.nodeId = nodeId;
    }

    public void Connect() {
      Console.WriteLine($"Connecting to node {nodeId}");
    }

    public void DoCommand(string command) {
      Console.WriteLine($"Command {command} for node {nodeId}");
    }

    public void Disconnect() {
      //Correct behavior is to call Dispose from Disconnect
      //Let's try it without it to see what happens :-)
    }

    public void Dispose()
    {
      if(!disposed) {
        disposed = true;
        Console.WriteLine($"Disposing command runner {nodeId}");
      }
    }
  }
}