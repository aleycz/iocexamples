﻿using System;

namespace Factory {
  public interface ICommandRunner : IDisposable {
    void Connect();
    void DoCommand(string command);
    void Disconnect();
  }
}