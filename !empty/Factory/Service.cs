﻿using System.Collections.Generic;

namespace Factory
{
    public class Service
    {
        public void DoPoll(IEnumerable<int> nodes)
        {
            foreach (int nodeId in nodes)
            {
                var runner = new CommandRunner(nodeId);

                runner.Connect();
                runner.DoCommand("...");
                runner.Disconnect();
            }
        }
    }
}
