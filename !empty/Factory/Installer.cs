﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Factory {
  public class Installer : IWindsorInstaller {
    public void Install(IWindsorContainer container, IConfigurationStore store) {
      container.Register(Component.For<ICommandRunner>().ImplementedBy<CommandRunner>().LifestyleTransient());
      container.Register(Component.For<Service>());
    }
  }
}