﻿using System.Collections.Generic;
using System.Linq;

namespace Plugins {
  public class MainService
  {
    private readonly IEnumerable<IPlugin> plugins;

    public MainService(IEnumerable<IPlugin> plugins) {
      this.plugins = plugins;
    }

    public void ProcessData(object data) {
      plugins.FirstOrDefault(p => p.IsFor(data))?.DoHardWork(data);
    }
  }
}